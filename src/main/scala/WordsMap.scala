import scala.collection.mutable.{Map => MutableMap}


class WordsMap{
	 private var _words = scala.collection.mutable.Map[String, Int]().withDefaultValue(0)	 
	 // Getter
     def words = _words
	 // Setter
     def words_= (value:MutableMap[String, Int]):Unit = _words = value
	 
	 def insert(word: String,count: Int): Unit = {
		 var wordCount = words(word)
		 wordCount+=count        
		 words += (word -> wordCount)
	 }
	 
	 def maximumWord(): String = {
		 var res = ""
		 var max = 0
		 for ((k,v) <- words){
			 if(v > max){
				 max = v
				 res = k
			 }
		 }
		 res
	  }	 
	  
	  def maximumWordCount(): Int = {
		  val word = maximumWord
		  words(word)
	  }
	  	 	 
 }
 
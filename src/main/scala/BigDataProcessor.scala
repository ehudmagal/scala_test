import java.io.RandomAccessFile
import java.io.FileWriter

class BigDataProcessor(bigDataFilePath: String) {
	val defaultBlockSize =  100
	val byteBuffer = new Array[Byte](defaultBlockSize)
	val randomAccessFile = new RandomAccessFile(bigDataFilePath, "r")
	val root_directory = System.getProperty("user.dir")
	val helper = new StringHelper
	
	def getNumberOfChunks(): Int = {	
    val randomAccessFile = new RandomAccessFile(bigDataFilePath, "r")
    try {
      ((randomAccessFile.length / defaultBlockSize) + 1).toInt
    } finally {
      randomAccessFile.close
    }
  }
  
  def getChunk(chunkIndex: Int): Unit = {
    val randomAccessFile = new RandomAccessFile(bigDataFilePath, "r")
    try {
      val seek = (chunkIndex - 1) * defaultBlockSize
      randomAccessFile.seek(seek)
      randomAccessFile.read(byteBuffer)     
    } finally {
      randomAccessFile.close
    }
  }
  
  def reverse_file(): Unit = {
	  val fw = new FileWriter(root_directory+"\\out.txt");
	  for  (i <- (1 to getNumberOfChunks()).reverse){ 
	    getChunk(i)
		fw.write(helper.bytesToString(byteBuffer).reverse);
	  }
	fw.close();	 
  }
  
 def mostFrequentWords(): Unit = {
	for (i <- 1 to 5) { 
		val map = MostFrequentWord(i)	
		println("most common word is " + map.maximumWord + " it appeared " + map.maximumWordCount + " times")
	}  
 }
  
 def MostFrequentWord(length: Int): WordsMap = {
    var wordsMap = new WordsMap()
	var tail = new Array[Byte](length - 1)	
    for (i <- 1 to getNumberOfChunks()) {
      getChunk(i)     
	  var searchArray =  byteBuffer
	  if(i > 1){
		 searchArray = tail ++ searchArray
	  }
	  for(j <- 0 until searchArray.length - length){
		var subArray = searchArray.slice(j,j+length)
		if(helper.isWord(subArray)){
	      val word = helper.bytesToString(subArray)
		  wordsMap.insert(word,1)
		}
      }
	 
	  tail = byteBuffer.slice(byteBuffer.length - tail.length, byteBuffer.length)
     }	 	
	 wordsMap
  }	 
        
 }
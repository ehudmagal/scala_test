class StringHelper{
	 	 
    def bytesToString(bytes: Array[Byte]): String = {
	  (bytes.map(_.toChar)).mkString 
    }
	
	def isWord(bytes: Array[Byte]): Boolean = {
	  	var res = true
		var i = 0
		while(i < bytes.length && res){
			if(!byteIsLetter(bytes(i))){
				res = false
			}
			i+=1
		}
		
		res
	}
	
	def byteIsLetter(byte: Byte): Boolean = {
	  ('a' to 'z')  union ('A' to 'Z') contains byte.toChar
    }
 }